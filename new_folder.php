<?php
require(__DIR__.'/app/application.php');

if(!is_logged_in()) {
    body_prepend(get_error('Vous n\'êtes pas connecté.'));
    redirect('login.php');
}

$path = (isset($_GET['path'])) ? $_GET['path'] : $user->default_dir;
if($path == '/') { $path = ''; }

if(isset($_POST['submit']) && !empty($_POST['folder_name'])) {
    $ftp_conn = get_ftp_conn();

    if (ftp_mkdir($ftp_conn, $path.'/'.$_POST['folder_name'])) {
        body_prepend(get_success('Le dossier <strong>'.$_POST['folder_name'].'</strong> a été créé avec succès.'));
    } else {
        body_prepend(get_error('Impossible de créer le dossier.'));
    }

    redirect('browser.php?path='.urlencode($path));
}

$titre = 'Créer un dossier';

$show_path = ($path != '') ? $path : '/';
$body .= '<p>Création du dossier dans : <strong>'.$show_path.'</strong>.';

$body .= '<br><a href="browser.php?path='.urlencode($path).'">Retour à l\'explorateur</a></p>';

$body .= file_get_contents(__DIR__.'/templates/new_folder.html');

include(__DIR__.'/templates/base.php');
