<?php
require(__DIR__.'/app/application.php');

if(!is_logged_in()) {
    body_prepend(get_error('Vous n\'êtes pas connecté.'));
    redirect('login.php');
}


$path = (isset($_GET['path'])) ? $_GET['path'] : $user->default_dir;
if($path == '/' or $path == '.') { $path = ''; }

$download = (isset($_GET['download'])) ? $_GET['download'] : False;

$delete = (isset($_GET['delete'])) ? $_GET['delete'] : False;

$dir = (isset($_GET['dir'])) ? (bool) $_GET['dir'] : False;

$ftp_conn = get_ftp_conn();


if($download) {
    stream_download_ftp($ftp_conn, $download);
}

if($delete) {
    $type = ($dir) ? 'répertoire' : 'fichier';

    if(remove_ftp($ftp_conn, $delete, $dir)) {
        body_prepend(get_success('Le '.$type.' <strong>'.$delete.'</strong> a bien été supprimé.'));
    } else {
        body_prepend(get_error('Une erreur est survenue lors de la suppression du '.$type.' <strong>'.$delete.'</strong>.'));
    }

    redirect('?path='.dirname($delete));
}


$files = ls_ftp($ftp_conn, $path);

$show_path = ($path != '') ? $path : '/';
$body .= '<p class="tac">';
$body .= 'Vous êtes ici : <strong>'.$show_path.'</strong>.';
$body .= '<a href="upload.php?path='.urlencode($path).'"><img class="icon" src="assets/img/upload.png" alt="Envoyer des fichiers" title="Envoyer des fichiers"></a>';
$body .= '<a href="new_folder.php?path='.urlencode($path).'"><img class="icon" src="assets/img/new_folder.png" alt="Créer un dossier" title="Créer un dossier"></a>';
$body .= '<a onclick="var file = prompt(\'Veuillez entrer un nom pour le fichier :\', \'nouveau_fichier\'); this.href = \'edit.php?file='.urlencode($path).'/\' + encodeURIComponent(file);" href="edit.php?file='.urlencode($path).'/nouveau_fichier"><img class="icon" src="assets/img/new_file.png" alt="Créer un fichier" title="Créer un fichier"></a>';
$body .= '</p>';

if($files !== False) {
    $body .= '<table>';
    $body .= '<tr><th id="th_filename">Nom</th><th>Droits</th><th>Actions</th><th>Taille</th><th>Dernière modification</th></tr>';

    if(!empty(dirname($path))) {
        $body .= '<tr><td colspan="6" class="tac"><a href="?path='.urlencode(dirname($path)).'"><img class="icon" src="assets/img/up.png" alt="Flèche vers le haut"> Vers '.dirname($path).'</a></td></tr>';
    }

    foreach($files as $filename => $file) {
        if($file['type'] == 'Répertoire') {
            $get_parameter = '?path='.urlencode($path.'/'.$filename);
            $human_taille = '';
            $dir_parameter = '&dir=1';
            $icon = 'folder.png';
            $class = 'folder';
        } else {
            $get_parameter = '?download='.urlencode($path.'/'.$filename);
            $human_taille = human_readable_bytes($file['taille']);
            $dir_parameter = '';
            $icon = 'file.png';
            $class = 'file';
        }

        $body .= '<tr>';

        $body .= '<td class="filename '.$class.'">';
        if($file['type'] == 'Répertoire') {
            $body .= '<a href="'.$get_parameter.'">';
        } else {
            $body .= '<a href="edit.php?file='.urlencode($path.'/'.$filename).'">';
        }
        $body .= '<img src="assets/img/'.$icon.'" class="icon">';
        $body .= $filename;
        $body .= '</a>';
        $body .= '</td>';

        $body .= '<td class="droits">'.$file['droits'].'</td>';

        $body .= '<td class="tac">';
        if($file['type'] == 'Fichier') {
            $body .= '<a href="'.$get_parameter.'"><img class="icon-sm" src="assets/img/download.png" alt="Télécharger" title="Télécharger"></a></a>';
        }
        $body .= '<a href="move.php?path='.urlencode($path.'/'.$filename).'"><img class="icon-sm" src="assets/img/move.png" alt="Déplacer / renommer" title="Déplacer / renommer"></a>';
        $body .= '<a onclick="return confirm(\'Voulez-vous vraiment supprimer ce '.strtolower($file['type']).' ?\')" href="browser.php?delete='.urlencode($path.'/'.$filename).$dir_parameter.'"><img class="icon-sm" src="assets/img/remove.png" alt="Supprimer" title="Supprimer"></a>';
        $body .='</td>';

        $body .= '<td>'.$human_taille.'</td>';

        $heure_annee = (strlen($file['heure']) == 5) ? 'à '.$file['heure']: $file['heure'];

        $body .= '<td>'.$file['jour'].' '.$file['mois'].' '.$heure_annee.'</td>';
        $body .= '</tr>';
    }

    $body .= '</table>';
} else {
    $body .= '<p>Aucun fichier ici !</p>';
}

$titre = 'Navigateur';
include(__DIR__.'/templates/base.php');
