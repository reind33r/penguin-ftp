<?php
require(__DIR__.'/config.php');
require(__DIR__.'/classes/User.php');

/* HELPERS HTTP */

function redirect($path) {
    header('Location: '.$path);
    exit();
}

function fatal_error($message) {
    http_response_code(500);
    include(__DIR__.'/../templates/http/fatal_error.php');
    exit();
}

function denied_access($message) {
    http_response_code(403);
    include(__DIR__.'/../templates/http/denied_access.php');
    exit();
}

function not_found($message) {
    http_response_code(404);
    include(__DIR__.'/../templates/http/not_found.php');
    exit();
}

/* FONCTIONS DE TEMPLATING */

function get_msg($type, $message, $titre='') {
    $titre_html = ($titre) ? '<h4>'.$titre.'</h4>' : '';

    return '<div class="'.$type.'">'.$titre_html.'<p>'.$message.'</p></div>';
}
function get_success($message, $titre='') {
    return get_msg('success', $message, $titre);
}
function get_error($message, $titre='') {
    return get_msg('error', $message, $titre);
}

/* SESSIONS */

session_start();

if (!is_writable(session_save_path())) {
    fatal_error('Le répertoire "'.session_save_path().'" n\'est pas accessible en écriture.');
}


function is_logged_in() {
    return isset($_SESSION['user']) && is_object($_SESSION['user']);
}

if(is_logged_in()) {
    $user = $_SESSION['user'];
}

$_SESSION['body_prepend'] = (isset($_SESSION['body_prepend'])) ? $_SESSION['body_prepend'] : '';

function body_prepend($data) {
    $_SESSION['body_prepend'] .= $data;
}

/* BODY */

$body = '';
$body .= $_SESSION['body_prepend'];

unset($_SESSION['body_prepend']);



/* FTP */
$ftp_conn_created = False;

function get_ftp_conn() {
    global $ftp_conn_created, $user;

    if($ftp_conn_created) {
        throw new ErrorException('Une connexion a déjà été créée.');
    }
    if(!is_logged_in()) {
        throw new ErrorException('La fonction get_ftp_conn() ne peut s\'utiliser que lorsque l\'utilisateur est connecté.');
    }

    $ftp_conn = ftp_ssl_connect(FTP_HOST);

    if(@ftp_login($ftp_conn, $user->username, $user->password)) {
        if(!ftp_pasv($ftp_conn, True)) {
            throw new Exception('Impossible d\'activer le mode passif.');
        }

        $ftp_conn_created = True;
        return $ftp_conn;
    } else {
        throw new ErrorException('Impossible de se connecter au serveur FTP avec les identifiants session.');
    }
}


function ls_ftp($ftp_conn, $directory) {
    if(!@ftp_chdir($ftp_conn, $directory) == -1) {
        not_found('Le répertoire <strong>'.$directory.'</strong> n\'existe pas sur le serveur.');
    }

    if (is_array($children = @ftp_rawlist($ftp_conn, $directory))) {
        $items = array();

        foreach ($children as $child) {
            $chunks = preg_split("/\s+/", $child);
            list($item['droits'], $item['nombre_liens'], $item['user'], $item['group'], $item['taille'], $item['mois'], $item['jour'], $item['heure']) = $chunks;
            $item['type'] = $chunks[0]{0} === 'd' ? 'Répertoire' : 'Fichier';
            array_splice($chunks, 0, 8);
            $items[implode(" ", $chunks)] = $item;
        }

        return $items;
    } else {
        return False;
    }
}

function stream_download_ftp($ftp_conn, $file) {
    if(ftp_mdtm($ftp_conn, $file) == -1) {
        not_found('Le fichier <strong>'.$file.'</strong> n\'existe pas sur le serveur.');
    }

    header('Content-disposition: attachment;filename="'.basename($file).'"');

    $sockets = stream_socket_pair(STREAM_PF_UNIX, STREAM_SOCK_STREAM, STREAM_IPPROTO_IP);

    if($sockets === FALSE) {
        throw new ErrorException('Impossible de créer le socket.');
    }

    stream_set_write_buffer($sockets[0], 0);
    stream_set_timeout($sockets[1], 0);

    try {
        $get_status = ftp_nb_fget($ftp_conn, $sockets[0], $file, FTP_BINARY);

        while($get_status === FTP_MOREDATA) {
            $contents = stream_get_contents($sockets[1]);

            if($contents !== false) {
                echo $contents;
                flush();
            }

            $get_status = ftp_nb_continue($ftp_conn);
        }

        $contents = stream_get_contents($sockets[1]);

        if($contents !== false) {
            echo $contents;
            flush();
            exit();
        }
    } catch(Exception $e) {
        fclose($sockets[0]);
        fclose($sockets[1]);

        throw $e;
    }
}

function upload_ftp($file, $ftp_conn, $to_path) {
    $fp = fopen($file, 'r');

    $ret = ftp_nb_fput($ftp_conn, $to_path, $fp, FTP_BINARY);
    while ($ret == FTP_MOREDATA) {
        echo '.';
        $ret = ftp_nb_continue($ftp_conn);
    }
    if ($ret != FTP_FINISHED) {
        fatal_error('Une erreur a eu lieu lors du chargement des fichiers sur le serveur.');
    }

    fclose($fp);
}

function upload_text_ftp($content, $ftp_conn, $to_file) {
    $stream = fopen('data://text/plain;base64,'.base64_encode($content), 'r');

    $ret = ftp_nb_fput($ftp_conn, $to_file, $stream, FTP_BINARY);
    while ($ret == FTP_MOREDATA) {
        echo '.';
        $ret = ftp_nb_continue($ftp_conn);
    }
    if ($ret != FTP_FINISHED) {
        fatal_error('Une erreur a eu lieu lors du chargement du fichier sur le serveur.');
    }

    fclose($stream);
}


function remove_ftp($ftp_conn, $path, $is_dir) {
    if($is_dir) {
        if(!@ftp_chdir($ftp_conn, $directory) == -1) {
            not_found('Le répertoire <strong>'.$directory.'</strong> n\'existe pas sur le serveur.');
        } else {
            $enfants = ls_ftp($ftp_conn, $path);
            foreach($enfants as $filename => $file) {
                $dir = ($file['type'] == 'Répertoire');
                remove_ftp($ftp_conn, $path.'/'.$filename, $dir);
            }

            return ftp_rmdir($ftp_conn, $path);
        }
    } else {
        if(ftp_mdtm($ftp_conn, $path) == -1) {
            not_found('Le fichier <strong>'.$path.'</strong> n\'existe pas sur le serveur.');
        } else {
            return ftp_delete($ftp_conn, $path);
        }
    }
}

function human_readable_bytes($bytes)
{
    if ($bytes == 0) {
        return "0.00 B";
    }

    $s = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
    $e = floor(log($bytes, 1024));

    return round($bytes/pow(1024, $e), 2).$s[$e];
}
