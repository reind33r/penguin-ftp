<?php
require(__DIR__.'/app/application.php');

if(is_logged_in()) {
    denied_access('Vous êtes déjà connecté !');
}

if(isset($_POST['username']) && isset($_POST['password'])) {
    $ftp_conn = ftp_ssl_connect(FTP_HOST);

    if(@ftp_login($ftp_conn, $_POST['username'], $_POST['password'])) {
        body_prepend(get_success('Vous êtes désormais connecté.', 'Connexion réussie'));

        $user = new User();
        $user->username = $_POST['username'];
        $user->password = $_POST['password'];

        $user->default_dir = ftp_pwd($ftp_conn);

        $_SESSION['user'] = $user;

        redirect('browser.php');
    } else {
        $body .= get_error('Une erreur est survenue lors de la connexion.', 'Échec lors de la connexion');
    }
}

$titre = 'Connexion';
$body .= file_get_contents(__DIR__.'/templates/login.html');
include(__DIR__.'/templates/base.php');
