<?php
require(__DIR__.'/app/application.php');

if(!is_logged_in()) {
    denied_access('Vous devez être connecté pour vous déconnecter !');
}

unset($_SESSION['user']);
body_prepend(get_success('Vous avez été déconnecté avec succès. Merci d\'utiliser le WebFTP Hostux.fr !', 'Au revoir'));
redirect('login.php');
