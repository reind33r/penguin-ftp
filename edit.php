<?php
require(__DIR__.'/app/application.php');

if(!is_logged_in()) {
    body_prepend(get_error('Vous n\'êtes pas connecté.'));
    redirect('login.php');
} if(!isset($_GET['file'])) {
    not_found('L\'argument "file" doit être spécifié.');
}

$file = $_GET['file'];

if(isset($_POST['content'])) {
    $ftp_conn = get_ftp_conn();

    upload_text_ftp($_POST['content'], $ftp_conn, $file);

    echo '{"status": "success"}';
    exit();
}

$ftp_conn = get_ftp_conn();

ob_start();
$result = @ftp_get($ftp_conn, "php://output", $file, FTP_BINARY);
$data = ob_get_contents();
ob_end_clean();

$body .= '<p id="hideJs">Vous devez activer JavaScript pour utiliser l\'éditeur.</p>';

$body .= '<form method="POST" style="display: none;">';

$body .= '<textarea name="contenu"></textarea>';

$body .= '</form>';

$body .= '<p id="controles"><strong>'.$file.'</strong>';
$body .= '<a onclick="save(); return false;" href="#"><img class="icon-sm" src="assets/img/save.png" alt="Sauvegarder" title="Sauvegarder"></a>';

$body .= '<span style="display: none;" id="save_in_progress">Sauvegarde en cours...</span>';
$body .= '<span style="display: none;" id="save_done">Sauvegarde effectuée !</span>';
$body .= '<span style="display: none;" id="save_error">La dernière sauvegarde a eu un problème.</span>';

$body .= '<br><a href="browser.php?path='.urlencode(dirname($file)).'">Retour à l\'explorateur</a></p>';



$body .= '<div id="editeur">'.htmlspecialchars($data).'</div>';


if(substr($file, -3) == 'css') {
    $ace_mode = 'css';
} elseif(substr($file, -5) == '.html' or substr($file, -4) == '.htm') {
    $ace_mode = 'html';
} elseif(substr($file, -5) == '.html' or substr($file, -4) == '.htm') {
    $ace_mode = 'html';
} elseif(substr($file, -4) == '.php') {
    $ace_mode = 'php';
} elseif(substr($file, -5) == '.json') {
    $ace_mode = 'json';
} elseif(substr($file, -3) == '.js') {
    $ace_mode = 'javascript';
}


$body .= '<script src="vendor/ace-builds/src-min/ace.js" type="text/javascript" charset="utf-8"></script>
<script>
    var editor = ace.edit("editeur");
    editor.setTheme("ace/theme/monokai");

    editor.commands.addCommand({
        name: "save",
        bindKey: {win: "Ctrl-S", mac: "Command-S"},
        exec: function(editor) {
            save();
        }
    });
';

if(isset($ace_mode)) {
    $body .= '
        editor.getSession().setMode("ace/mode/'.$ace_mode.'");';
}
$body .= '
        document.getElementById(\'hideJs\').style.display = \'none\';

        document.getElementById(\'controles\').style.zIndex = \'1000\';
        document.getElementById(\'controles\').style.position = \'absolute\';
        document.getElementById(\'controles\').style.top = 0;
        document.getElementById(\'controles\').style.left = 0;
        document.getElementById(\'controles\').style.height = \'5rem\';
        document.getElementById(\'controles\').style.width = \'100%\';
        document.getElementById(\'controles\').style.margin = \'0\';
        document.getElementById(\'controles\').style.padding = \'1rem\';
        document.getElementById(\'controles\').style.background = \'white\';

        function save() {
            document.getElementById(\'save_in_progress\').style.display = \'inline\';

            xhttp = new XMLHttpRequest();

            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById(\'save_in_progress\').style.display = \'none\';
                    document.getElementById(\'save_error\').style.display = \'none\';
                    document.getElementById(\'save_in_progress\').style.display = \'none\';
                    document.getElementById(\'save_done\').style.display = \'inline\';

                    setTimeout(function() {
                        document.getElementById(\'save_done\').style.display = \'none\';
                    }, 5000);
                } else if(this.readyState == 4) {
                    document.getElementById(\'save_in_progress\').style.display = \'none\';
                    document.getElementById(\'save_error\').style.display = \'inline\';
                }
            };
            xhttp.open("POST", "edit.php?file='.urlencode($file).'", true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.send("content=" + encodeURIComponent(editor.getValue()));
        }
    </script>';

$titre = 'Éditeur';
include(__DIR__.'/templates/base.php');
