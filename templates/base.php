<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Hostux.fr - Penguin FTP</title>

        <link rel="stylesheet" href="assets/design.css">
    </head>
    <body>
        <header>
            <h1><img src="assets/img/PenguinFTP.png" alt="Penguin FTP: a web-based FTP browser"></h1>
            <h2><?php echo $titre; ?></h2>

            <?php
            if(is_logged_in()) {
                echo '<p><a href="logout.php">Déconnexion</a></p>';
            }
            ?>
        </header>

        <?php
        echo $body;
        ?>
    </body>
    <footer>
        <p>
            Penguin FTP, une réalisation <a href="https://louis.hostux.fr">Louis Guidez</a>.<br>
            <a href="https://bitbucket.org/reind33r/penguin-ftp/src">Voir la source</a>
        </p>
    </footer>
</html>
