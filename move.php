<?php
require(__DIR__.'/app/application.php');

if(!is_logged_in()) {
    body_prepend(get_error('Vous n\'êtes pas connecté.'));
    redirect('login.php');
} if(!isset($_GET['path'])) {
    not_found('L\'argument "path" doit être spécifié.');
}

$path = $_GET['path'];

if(isset($_POST['new_path'])) {
    $ftp_conn = get_ftp_conn();

    if(@ftp_rename($ftp_conn, $path, $_POST['new_path'])) {
        body_prepend(get_success('Déplacement effectué.'));
        redirect('browser.php?path='.urlencode(dirname($path)));
    } else {
        $body .= get_error('Une erreur a eu lieu lors du déplacement. Vérifiez que le dossier de destination existe.');
    }
}

$titre = 'Déplacer / renommer';

$show_path = ($path != '') ? $path : '/';
$body .= '<p>Déplacement de : <strong>'.$show_path.'</strong>. <a href="browser.php?path='.urlencode(dirname($path)).'">Retour au navigateur</a></p>';

$value_new_path = (isset($_POST['new_path'])) ? $_POST['new_path'] : htmlspecialchars($show_path);

$body .= '<form method="POST">
    <div class="field">
        <label for="new_path">Nouveau nom</label>
        <input type="text" name="new_path" id="new_path" value="'.$value_new_path.'">
    </div>
    <div class="submit">
        <input type="submit" value="Déplacer / renommer">
    </div>
</form>
';

include(__DIR__.'/templates/base.php');
