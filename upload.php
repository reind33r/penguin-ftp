<?php
require(__DIR__.'/app/application.php');

if(!is_logged_in()) {
    body_prepend(get_error('Vous n\'êtes pas connecté.'));
    redirect('login.php');
}

$path = (isset($_GET['path'])) ? $_GET['path'] : $user->default_dir;
if($path == '/') { $path = ''; }

if(isset($_POST['submit']) && !empty($_FILES['fichiers']['name'][0])) {
    $ftp_conn = get_ftp_conn();

    // TODO : ini_get(max_file_uploads) et check <

    foreach($_FILES['fichiers']['name'] as $file => $name) {
        if($_FILES['fichiers']['error'][$file] != UPLOAD_ERR_OK) {
            body_prepend(get_error('Le fichier <strong>'.$name.'</strong> n\'a pas été téléchargé (code d\'erreur n°'.$_FILES['fichiers']['error'][$file].').'));
        }
        upload_ftp($_FILES['fichiers']['tmp_name'][$file], $ftp_conn, $path.'/'.$name);
        body_prepend(get_success('Le fichier <strong>'.$name.'</strong> a été envoyé avec succès.'));
    }

    redirect('browser.php?path='.urlencode($path));
}

$titre = 'Envoyer des fichiers';

$show_path = ($path != '') ? $path : '/';
$body .= '<p>Envoi des fichiers à : <strong>'.$show_path.'</strong>. <a href="browser.php?path='.urlencode($path).'">Retour au navigateur</a></p>';

$body .= file_get_contents(__DIR__.'/templates/upload.html');

$body .= '<p>Attention : vous ne pouvez envoyer que '.ini_get('max_file_uploads').' fichiers à la fois.';

$body .= '<script type="text/javascript">
function checkFichiers() {
    input = document.getElementById(\'fichiers\');

    if(input.files.length >= '.ini_get('max_file_uploads').') {
        alert(\'Vous ne pouvez pas envoyer plus de '.ini_get('max_file_uploads').' fichiers.\');
        return false;
    }
    return true;
}
</script>
';

include(__DIR__.'/templates/base.php');
